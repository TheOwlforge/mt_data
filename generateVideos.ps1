$dir = dir �.\screenshots\light\� | ?{$_.PSISContainer}
mkdir .\videos\light\

foreach ($d in $dir){
    $out = ".\videos\light\" + $d.Name + ".mp4"

    $path = Join-Path -Path $d.FullName -ChildPath "ejecta_%03d.png"
    & .\ffmpeg.exe -framerate 10 -i $path $out

    $path = Join-Path -Path $d.FullName -ChildPath "present_%03d.png"
    & .\ffmpeg.exe -framerate 10 -i $path $out

    $path = Join-Path -Path $d.FullName -ChildPath "thorax_%03d.png"
    & .\ffmpeg.exe -framerate 10 -i $path $out
}

$dir = dir �.\screenshots\camera\� | ?{$_.PSISContainer}
mkdir .\videos\camera

foreach ($d in $dir){
    $out = ".\videos\camera\" + $d.Name + ".mp4"

    $path = Join-Path -Path $d.FullName -ChildPath "ejecta_%03d.png"
    & .\ffmpeg.exe -framerate 10 -i $path $out

    $path = Join-Path -Path $d.FullName -ChildPath "present_%03d.png"
    & .\ffmpeg.exe -framerate 10 -i $path $out

    $path = Join-Path -Path $d.FullName -ChildPath "thorax_%03d.png"
    & .\ffmpeg.exe -framerate 10 -i $path $out
}