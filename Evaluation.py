import lpips
import torch
import os
import gc

ref_dir = './screenshots/camera/ejecta_ref/'
data_dir = './screenshots/camera/ejecta_'

methods = ["none", "bilat", "nlm", "perc_25", "perc_50", "perc_75",
           "regNF", "regNFNW", "regNW", "regVC",
           "reg_fst_bilat","reg_rep_mask_perc_95",
           "taa_fst_bilat_50", "taa_fst_perc_50", "taaW_fst_mask_bilat", "taaW_fst_mask_perc",
           "reg_dvr", "reg_dvr_mask", "reg_fst", "reg_fst_mask", "reg_rep", "reg_rep_mask",
           "taa_dvr", "taa_dvr_mask", "taa_fst", "taa_fst_mask", "taa_rep", "taa_rep_mask",
           "taaW_dvr", "taaW_dvr_mask", "taaW_fst", "taaW_fst_mask", "taaW_rep", "taaW_rep_mask",
           "taa_fst_50", "taa_fst_25"]

loss_fn_alex = lpips.LPIPS(net='alex')
loss_fn_alex.cuda()

distances = []
for m in methods:
    print(m)
    current_data_dir = data_dir + m + '/'
    files = os.listdir(current_data_dir)
    distance = []
    counter = 0
    for file in files:
        if os.path.exists(os.path.join(ref_dir, file)):
            ref = lpips.im2tensor(lpips.load_image(os.path.join(ref_dir, file)))
            img = lpips.im2tensor(lpips.load_image(os.path.join(current_data_dir, file)))

            ref = ref.cuda()
            img = img.cuda()

            d = float(loss_fn_alex(img, ref))
            distance.append(d)
            counter += 1

            print(torch.cuda.memory_reserved())
            del ref
            del img
            gc.collect()
            torch.cuda.empty_cache()
            print(torch.cuda.memory_reserved())
    gc.collect()
    torch.cuda.empty_cache()

    distances.append(distance)

f = open('metrics_LPIPS.csv', 'w')
numFrames = len(distances[0])

for m in range(0, len(methods)):
    f.writelines('%s' % methods[int(m)])
    if (m < len(methods) - 1):
        f.writelines(', ')
    else:
        f.writelines('\n')

for frame in range(0, numFrames):
    for method in range(0, len(distances)):
        d = distances[int(method)][int(frame)]
        f.writelines('%.6f' % d)
        if (method < len(methods) - 1):
            f.writelines(', ')
        else:
            f.writelines('\n')
f.close()
