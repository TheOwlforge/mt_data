# About

This repository contains scripts and files I used for the evaluation part of my Master's thesis (https://gitlab.com/TheOwlforge/basic-volume-renderer).

The power shell scripts are used to convert multiple screenshots containing the single frames of an animation showing a volume rendering using VPT to a video. The image files are not on github for space reasons. The python script is used to compute the LPIPS metric, all other metrics are computed using Matlab. The results are outputed in .csv format and futher processed in the R scipt. The plots are exported as .pdf for faster import to Overleaf.

The final thesis can be found at: https://www.overleaf.com/read/kmdvkdxmgbgw

<p align="center">
    <img src="images/plot1.png" width="300" title="Comparison of different denoising techniques">
    <img src="images/plot2.png" width="300" title="Effect of TAA strenghts">
</p>
