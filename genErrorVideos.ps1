mkdir .\videos\camera_error

$dir = dir '.\results\ejecta_out_c\errorImages\' | ?{$_.PSISContainer}
foreach ($d in $dir){
    $out = ".\videos\camera_error\ejecta_" + $d.Name + "_error.mp4"
    $filename = "error_" + $d.Name + "_%03d.png"
    $path = Join-Path -Path $d.FullName -ChildPath $filename
    & .\ffmpeg.exe -framerate 10 -i $path $out
}
$dir = dir '.\results\thorax_out_c\errorImages\' | ?{$_.PSISContainer}
foreach ($d in $dir){
    $out = ".\videos\camera_error\thorax_" + $d.Name + "_error.mp4"
    $filename = "error_" + $d.Name + "_%03d.png"
    $path = Join-Path -Path $d.FullName -ChildPath $filename
    & .\ffmpeg.exe -framerate 10 -i $path $out
}
$dir = dir '.\results\present_out_c\errorImages\' | ?{$_.PSISContainer}
foreach ($d in $dir){
    $out = ".\videos\camera_error\present_" + $d.Name + "_error.mp4"
    $filename = "error_" + $d.Name + "_%03d.png"
    $path = Join-Path -Path $d.FullName -ChildPath $filename
    & .\ffmpeg.exe -framerate 10 -i $path $out
}

mkdir .\videos\light_error

$dir = dir '.\results\ejecta_out_l\errorImages\' | ?{$_.PSISContainer}
foreach ($d in $dir){
    $out = ".\videos\light_error\ejecta_" + $d.Name + "_error.mp4"
    $filename = "error_" + $d.Name + "_%03d.png"
    $path = Join-Path -Path $d.FullName -ChildPath $filename
    & .\ffmpeg.exe -framerate 10 -i $path $out
}
$dir = dir '.\results\thorax_out_l\errorImages\' | ?{$_.PSISContainer}
foreach ($d in $dir){
    $out = ".\videos\light_error\thorax_" + $d.Name + "_error.mp4"
    $filename = "error_" + $d.Name + "_%03d.png"
    $path = Join-Path -Path $d.FullName -ChildPath $filename
    & .\ffmpeg.exe -framerate 10 -i $path $out
}
$dir = dir '.\results\present_out_l\errorImages\' | ?{$_.PSISContainer}
foreach ($d in $dir){
    $out = ".\videos\light_error\present_" + $d.Name + "_error.mp4"
    $filename = "error_" + $d.Name + "_%03d.png"
    $path = Join-Path -Path $d.FullName -ChildPath $filename
    & .\ffmpeg.exe -framerate 10 -i $path $out
}